package it.unipd.dei.webapp.resource;

public class Members {
    /**
     * The badge number (identifier) of the member
     */
    private final int memberid;

    /**
     * The name of the member
     */
    private final String membername;

    /**
     * The department of the member
     */
    private final String department;

    /**
     * The password of the member
     */
    private final String password;

    public Members(final int memberid, final String membername, final String department, final String password) {
        this.memberid = memberid;
        this.membername = membername;
        this.department = department;
        this.password = password;
    }

    /**
     * Returns the  memberID of the member.
     *
     * @return the memberID of the member.
     */
    public final int getMemberid() {
        return memberid;
    }

    /**
     * Returns the name of the member.
     *
     * @return the name of the member.
     */
    public final String getMembername() {
        return membername;
    }


    /**
     * Returns the department of the member.
     *
     * @return the department of the member.
     */
    public final String getDepartment() {
        return department;
    }

   /* Returns the password of the member
    */

    public final String getPassword(){return password;}


}
