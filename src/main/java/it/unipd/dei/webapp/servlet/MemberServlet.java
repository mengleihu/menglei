package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreatememberDatabase;
import it.unipd.dei.webapp.resource.Members;
import it.unipd.dei.webapp.resource.Message;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/member")
public class MemberServlet extends AbstractDtabaseServlet {
    Members e  = null;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");
            String op = request.getParameter("op");
            if (null == op) {
                List<Members> list = new CreatememberDatabase(getDataSource().getConnection(), e).memberList();
                request.setAttribute("list", list);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            } else if ("del".equals(op)) {
                Integer memberid = Integer.parseInt(request.getParameter("memberid"));
                new CreatememberDatabase(getDataSource().getConnection(), e).delMember(memberid);
                List<Members> list = new CreatememberDatabase(getDataSource().getConnection(), e).memberList();
                request.setAttribute("list", list);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}