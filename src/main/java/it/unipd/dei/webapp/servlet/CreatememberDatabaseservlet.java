package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreatememberDatabase;
import it.unipd.dei.webapp.resource.Members;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreatememberDatabaseservlet extends AbstractDtabaseServlet{
    /**
     * Creates a new member into the database.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // request parameters
        int memberid = -1;
        String membername = null;
        String department = null;
        String password = null;

        // model
        Members e  = null;
        Message m = null;

        try{
            // retrieves the request parameters
            memberid = Integer.parseInt(req.getParameter("memberid"));
            membername = req.getParameter("membername");
            department = req.getParameter("department");
            password = req.getParameter("password");


            // creates a new member from the request parameters
            e = new Members(memberid, membername, department, password);

            // creates a new object for accessing the database and stores the member
            new CreatememberDatabase(getDataSource().getConnection(), e).createMember();

            m = new Message(String.format("Member %s successfully created.", memberid));

        } catch (NumberFormatException ex) {
           m = new Message("Cannot create the member. Invalid input parameters: memberid, name, department and password memberid must be integer.",
                  "E102", ex.getMessage());
        } catch (SQLException ex) {
           if (ex.getSQLState().equals("23505")) {
            m = new Message(String.format("Cannot create the member: employee %s already exists.", memberid),
                      "E105", ex.getMessage());
            } else {
               m = new Message("Cannot create the member: unexpected error while accessing the database.",
                       "E110", ex.getMessage());
            }
        }



        // set the MIME media type of the response
        res.setContentType("text/html; charset=utf-8");

        // get a stream to write the response
        PrintWriter out = res.getWriter();

        // write the HTML page
        out.printf("<!DOCTYPE html>%n");

        out.printf("<html lang=\"en\">%n");
        out.printf("<head>%n");
        out.printf("<meta charset=\"utf-8\">%n");
        out.printf("<title>Create Member</title>%n");
        out.printf("</head>%n");

        out.printf("<body>%n");
        out.printf("<h1>Create Member</h1>%n");
        out.printf("<hr/>%n");

       if(m.isError()) {
            out.printf("<ul>%n");out.printf("<li>error code: %s</li>%n", m.getErrorCode());
           out.printf("<li>message: %s</li>%n", m.getMessage());
            out.printf("<li>details: %s</li>%n", m.getErrorDetails());
            out.printf("</ul>%n");
        } else {
            out.printf("<p>%s</p>%n", m.getMessage());
            out.printf("<ul>%n");
            out.printf("<li>badge: %s</li>%n", e.getMemberid());
            out.printf("<li>surname: %s</li>%n", e.getMembername());
            out.printf("<li>department: %s</li>%n", e.getDepartment());
            out.printf("<li>password: %s</li>%n", e.getPassword());

           // out.printf("<li>salary: %s</li>%n", e.getSalary());
            out.printf("</ul>%n");
        }

        out.printf("</body>%n");

        out.printf("</html>%n");

        // flush the output stream buffer
        out.flush();

        // close the output stream
        out.close();


    }}
