package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Members;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class CreatememberDatabase {

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "INSERT INTO sy.members (memberid, membername, department,password) VALUES ( ?, ?, ?,?)";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The member to be stored into the database
     */
    private final Members members;

    /**
     * Creates a new object for storing a member into the database.
     *
     * @param con
     *            the connection to the database.
     * @param member
     *            the member to be stored into the database.
     */
    public CreatememberDatabase(final Connection con, final Members member) {
        this.con = con;
        this.members = member;
    }

    /**
     * Stores a new member into the database
     *
     * @throws SQLException
     *             if any error occurs while storing the member.
     */
    public void createMember() throws SQLException {

        PreparedStatement pstmt = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, members.getMemberid());
            pstmt.setString(2, members.getMembername());
            pstmt.setString(3, members.getDepartment());
            pstmt.setString(4, members.getPassword());



            pstmt.execute();

        } finally {
            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

    }

    public List<Members> memberList() throws SQLException {
        List<Members> list = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs;
        String sql = "SELECT * FROM sy.members";
        try {
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            Members members = null;
            while(rs.next()){
                members = new Members(rs.getInt(1),rs.getString(2),rs.getString(3),
                        rs.getString(4));
                list.add(members);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            con.close();
        }
        return list;
    }

    public void delMember(int memberid) throws SQLException {
        String sql = "delete FROM sy.members where memberid=?";
        PreparedStatement pstmt = null;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1,memberid);
            pstmt.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }  finally {
            if (pstmt != null) {
                pstmt.close();
            }
            con.close();
        }

    }
}
