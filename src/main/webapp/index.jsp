<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>memberlist</title>
  </head>
  <body>
    <h1>memberlist</h1>
    <a href="html/createmember.html">createmember</a>
    <table width="100%" border="1">
      <tr>
        <th>memberid</th>  <th>membername</th>  <th>department</th>
      </tr>
      <c:forEach items="${list}" var="member">
        <tr>
          <td>${member.memberid}</td>
          <td>${member.membername}</td>
          <td>${member.department}</td>
          <td>
              <a href="member?op=del&memberid=${member.memberid}">删除</a>
          </td>
        </tr>
      </c:forEach>
    </table>
  </body>
</html>